# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.7.0](https://gitlab.esss.lu.se/icshwi/reftabs/tags/1.7.0) - 2020-12-16

<small>[Compare with 1.6.1](https://gitlab.esss.lu.se/icshwi/reftabs/compare/1.6.1...1.7.0)</small>

### Added
- Add .gitlab-ci.yml to deploy reftabs ([c5927f2](https://gitlab.esss.lu.se/icshwi/reftabs/commit/c5927f2451c4ae5b1559ca9854b537514be9fadf) by Benjamin Bertrand).

### Fixed
- Fix: 2hz instead of 1hz ([faf1163](https://gitlab.esss.lu.se/icshwi/reftabs/commit/faf11634cc277d371e6c5626ce82b8d0c92b1889) by Jerzy Jamroz).
- Fix: fbis meeting agreements added ([7858e8b](https://gitlab.esss.lu.se/icshwi/reftabs/commit/7858e8b2ba0c648773182f1cd4577a3ee64f3a85) by Jerzy Jamroz).
- Fix: pbstate without fault and recovery ([5a4a20d](https://gitlab.esss.lu.se/icshwi/reftabs/commit/5a4a20dc84418e6834d81397dac3b206789d0a23) by Jerzy Jamroz).
- Fix: event names - update ([d722748](https://gitlab.esss.lu.se/icshwi/reftabs/commit/d7227482486428a127813a57adf97a33701b8739) by Jerzy Jamroz).
- Fix pipeline ([aa2ae1a](https://gitlab.esss.lu.se/icshwi/reftabs/commit/aa2ae1a4868b1630af8d7e80b3d7eb1f30929388) by Benjamin Bertrand).
- Fix: branch refers with bvx.x.x to the tag number. ([30dea3f](https://gitlab.esss.lu.se/icshwi/reftabs/commit/30dea3f0d47ea1bd7d7d894a6e08932d9577de47) by jerzyjamroz).


## [1.6.1](https://gitlab.esss.lu.se/icshwi/reftabs/tags/1.6.1) - 2020-04-09

<small>[Compare with first commit](https://gitlab.esss.lu.se/icshwi/reftabs/compare/09667be334d6427694955b167524b5d3470eabcc...1.6.1)</small>

### Fixed
- Fix: dtl1 is removed from ncl set because dtl2fc is sufficient. ([e0d2237](https://gitlab.esss.lu.se/icshwi/reftabs/commit/e0d2237ca165fb9742ca29326ad69211910e3cf7) by Jerzy Jamroz).
- Fix: pbdest refers to table 8 of ess-0038258 revision 6. ([cfd6d53](https://gitlab.esss.lu.se/icshwi/reftabs/commit/cfd6d536d62458312f24afe297e67adf69bcc3ab) by Jerzy Jamroz).
- Fix: pbmod updated according to table 5 of ess-0038258 revision 6. ([b983db6](https://gitlab.esss.lu.se/icshwi/reftabs/commit/b983db613130ceb9ced11bd6bca6f279dc99ec8b) by Jerzy Jamroz).


