# Init Tables

----

# Introduction
Tables to initialize settings around different tools.
# Major changes

## BState

Removed states from the databuffer.

```yaml
BState:
  Fault:
    id: 30
    description: "Fault detected, no proton beam."
  Recovery:
    id: 40
    description: "Recovering from a fault, proton beam for tune up. Possible neutron production, but not intended for users."
```

# Obsolence
The following files will not be falid anymore after the upade in 2024.
* databuffer-ess.yml
* sceconfig-ess.yml
