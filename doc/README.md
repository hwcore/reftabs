## Complementary Documentation

* [Timing Data Model Specification](https://confluence.esss.lu.se/display/HAR/Timing+Data+Model+Specification)
* [State Machine for ESS](StateMachineESS.md)
* [Allowed combinations of beam modes and insertable devices](https://chess.esss.lu.se/enovia/link/ESS-0377547/21308.51166.38912.2677/valid)
* [Timing Operator OPI](https://chess.esss.lu.se/enovia/link/ESS-2162066/21308.51166.48128.29321/valid)
