State Machine
---

# Beam OFF/ Beam ON – Button and functionality
 
## Timing OPI is used to request Beam OFF (by using beam off/on button) and then:
* TS stops sending triggers to magnetron PS
* TS distributes beam present = OFF
* TS distributes Beam OFF

## Timing OPI is used to request Beam ON (by using beam off/on button) and then:
* TS starts sending triggers to magnetron PS
* TS distributes beam present = ON
* TS distributes Beam ON
 
## Beam Mode “No beam” is selected:
* TS stops sending triggers to magnetron PS
* TS distributes beam present = OFF
* TS distributes beam mode

# FBIS enters Beam Inhibit State – 
1. FBIS stops beam by triggering the Beam Switch off Actuators
2. FBIS sends “TDS Interlock Trigger Request” to TS to stop Magnetron triggers (through DLN backplane to EVR) (Considered as BPCS - Secondary Beam Shut off Path in FBIS) 
3. TS stops sending triggers to magnetron PS
4. TS distributes beam present = OFF to all systems
5. FBIS leaves Beam Inhibit State (Automatic) – 
6. FBIS removes signal “TDS Interlock Trigger Request” to TS to stop Magnetron triggers through DLN backplane 
7. TS automatically starts sending triggers to magnetron PS
8. TS distributes beam present = ON to everyone

*Note: TS (timing OPI/ beam off button) will request a beam inhibit from FBIS as well, this will happen by TS changing Beam OFF/ON State to Beam OFF. This signal is received by FBIS through sw interlock input (PV is beam OFF and sent by timing IOC).*

# FBIS enters Regular Beam Interlock State or Emergency Beam Interlock State – 
1. FBIS stops beam by triggering the Beam Switch off Actuators
2. FBIS sends “TDS Interlock Trigger Request” to TS to stop Magnetron triggers (through DLN backplane to EVR) (Considered as BPCS - Secondary Beam Shut off Path in FBIS)
3. FBIS sends signal “TDS Post Mortem Request” to TS to issue post Mortem trigger (through DLN backplane to EVR) 
4. TS sends Post Mortem Trigger to all systems upon receiving (“TDS Post Mortem Request”) 
5. TS stops sending triggers to magnetron PS 
6. TS distributes beam present = OFF to all systems
7. FBIS is Reset by the operator -> FBIS leaves Beam Interlock State –
8. FBIS removes signal “TDS Interlock Trigger Request” to TS to stop Magnetron triggers through DLN backplane 
9. TS automatically starts sending triggers to magnetron PS
10. TS distributes beam present = ON to everyone
